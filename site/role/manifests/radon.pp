# == Class: role::radon
#
# Role for radon-like servers
#
class role::radon {
  include profile::base

  include profile::nginx
  include profile::docker
  include profile::media
  include profile::gitserver
  include profile::deluge
  include profile::userdirs

  include profile::services::files
  include profile::services::huginn
  include profile::services::kelvin
  include profile::services::plex
  include profile::services::radarr
  include profile::services::sonarr
}
