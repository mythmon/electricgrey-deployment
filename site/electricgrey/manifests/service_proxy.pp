# == Define: electricgrey::service_proxy
#
# Creates a proxy definition for a standard service
#
define electricgrey::service_proxy (
  Integer $port,
  String $domain = "${title}.electricgrey.com",
  Optional[String] $htpasswd_contents = undef,
) {
  include stdlib

  $htpasswd_path = "/etc/nginx/htpasswd.d/${title}"
  $certbot_webroot = "/etc/certbot/webroots/${domain}"

  nginx::resource::upstream { $title:
    members => ["localhost:${port}"],
  }

  nginx::resource::server { "${domain}-http":
    server_name          => [$domain],
    listen_port          => 80,
    use_default_location => false,
  }

  nginx::resource::location { "${domain} http redirect":
    server              => "${domain}-http",
    location            => '/',
    location_custom_cfg => {
      return => '301 https://$host$request_uri',
    },
  }

  nginx::resource::server { "${domain}-https":
    server_name          => [$domain],
    auth_basic           => if $htpasswd_contents { $domain },
    auth_basic_user_file => if $htpasswd_contents { $htpasswd_path },
    require              => if $htpasswd_contents { File["${title} htpasswd"] },
    proxy                => "http://${title}",
    proxy_http_version   => '1.1',
    proxy_set_header     => [
      'Upgrade $http_upgrade',
      'Connection "upgrade"',
      'X-Real-IP $remote_addr',
      'X-Forwarded-For $proxy_add_x_forwarded_for',
      'Host $http_host',
    ],
    proxy_redirect       => 'off',

    # Set this to 443 supresses the http version of the server
    listen_port          => 443,

    ssl                  => true,
    ssl_cert             => "/etc/letsencrypt/live/${domain}/fullchain.pem",
    ssl_key              => "/etc/letsencrypt/live/${domain}/privkey.pem",
    ssl_redirect         => true,
    ssl_protocols        => 'TLSv1.2',
    ssl_dhparam          => '/etc/nginx/dhparams.pem',
    ssl_session_timeout  => '1d',
    ssl_stapling         => true,
    ssl_stapling_verify  => true,
  }

  file { "${title} htpasswd":
    ensure  => if $htpasswd_contents { 'file' } else { 'absent' },
    path    => $htpasswd_path,
    owner   => 'http',
    mode    => '0700',
    content => $htpasswd_contents,
  }

  certbot::certonly { $domain: }
}
