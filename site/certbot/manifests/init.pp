# == Class: certbot
#
#   This class installs and configures the Let's Encrypt client.
#
# === Parameters:
#
class certbot (String $email) {
  package { 'certbot':
    ensure => present,
  }

  file { '/etc/letsencrypt':
    ensure    => directory,
  }

  file { '/etc/letsencrypt/webroots':
    ensure    => directory,
  }

  $command = '/usr/bin/certbot'
}
