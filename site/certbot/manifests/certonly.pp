# == Defined Type: certbot::certonly
#
#   This type can be used to request a certificate using the `certonly`
#   installer and the standard webroot plugin. Requires Proper Nginx
#   cooperation.
#
# === Parameters:
#
# [*domains*]
#   Namevar. The domain to include in the CSR.
#
define certbot::certonly (
  String $domain = $title,
) {
  $webroot_path = "/etc/letsencrypt/webroots/${domain}"
  $live_path = "/etc/certbot/live/${domain}/cert.pem"

  $command = @("HERE"/L)
    /usr/bin/systemctl start nginx
    ${::certbot::command} \
      --text \
      --non-interactive \
      --keep-until-expiring \
      --agree-tos certonly \
      -m ${::certbot::email} \
      --webroot \
      --webroot-path ${webroot_path} \
      -d ${domain}
    | HERE

  exec { "certbot certonly ${title}":
    command => $command,
    creates => $live_path,
    before  => Nginx::Resource::Server["${domain}-https"],
    require => [
      Class['certbot'],
      Nginx::Resource::Server["${domain}-http"],
      Nginx::Resource::Location["${domain} certbot webroot"],
      File[$webroot_path],
    ],
  }

  nginx::resource::location { "${domain} certbot webroot":
    server   => "${domain}-http",
    location  => '/.well-known/acme-challenge',
    www_root  => $webroot_path,
    require   => File[$webroot_path],
    priority  => 490,
    autoindex => 'on',
  }

  file { $webroot_path:
    ensure => directory,
  }
}
