# == Class: profile::userdirs
#
# Creates a webroot definition for a user dirs
#
class profile::userdirs (
  String $domain = "${title}.electricgrey.com",
) {
  include stdlib

  $htpasswd_path = "/etc/nginx/htpasswd.d/${title}"
  $certbot_webroot = "/etc/certbot/webroots/${domain}"

  nginx::resource::server { "${domain}-http":
    server_name          => [$domain],
    listen_port          => 80,
    use_default_location => false,
  }

  nginx::resource::location { "${domain} http redirect":
    server              => "${domain}-http",
    location            => '/',
    location_custom_cfg => {
      return => '301 https://$host$request_uri',
    },
  }

  nginx::resource::server { "${domain}-https":
    server_name          => [$domain],

    # Set this to 443 supresses the http version of the server
    listen_port          => 443,

    ssl                  => true,
    ssl_cert             => "/etc/letsencrypt/live/${domain}/fullchain.pem",
    ssl_key              => "/etc/letsencrypt/live/${domain}/privkey.pem",
    ssl_redirect         => true,
    ssl_protocols        => 'TLSv1.2',
    ssl_dhparam          => '/etc/nginx/dhparams.pem',
    ssl_session_timeout  => '1d',
    ssl_stapling         => true,
    ssl_stapling_verify  => true,
  }

  nginx::resource::location { "${domain} user dir":
    server         => "${domain}-https",
    location       => '/~([^/]+)($|/.*)$',
    location_alias => '/home/$1/public_html$2',
    autoindex      => 'on',
    ssl_only       => true,
  }

  certbot::certonly { $domain: }
}
