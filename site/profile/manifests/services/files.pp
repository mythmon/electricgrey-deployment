# == Class: profile::services::files
#
# Profile for a file server via HTTP.
#
class profile::services::files (
  Optional[String] $htpasswd_contents = undef,
) {

  electricgrey::service_webroot { 'files':
    webroot_path      => '/data',
    htpasswd_contents => $htpasswd_contents,
  }
}
