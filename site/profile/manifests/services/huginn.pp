# == Class: profile::services::huginn
#
# Profile for a Huginn server running in Docker, proxied through Nginx.
#
class profile::services::huginn (
  String $invitation_code = undef,
  String $smtp_user_name = undef,
  String $smtp_password = undef,
  String $email_from_address = undef,
) {

  docker_network { 'huginn-net':
    ensure => present,
    driver => 'bridge'
  }

  docker::image { 'postgres':
    image_tag => '9.4',
  }

  docker::run { 'huginn-db':
    image => 'postgres',
    net   => 'huginn-net',
    volumes => [
      '/var/lib/postgresql/data',
    ]
  }

  docker::image { 'huginn/huginn':
    image_tag => 'd58d002c27dd350a0f9113175362d81a1a16d851',
  }

  docker::run { 'huginn-web':
    image   => 'huginn/huginn',
    ports   => ['3000:3000'],
    net     => 'huginn-net',
    env     => [
      "INVITATION_CODE=${invitation_code}",
      'SMTP_DOMAIN=gmail.com',
      "SMTP_USER_NAME=${smtp_user_name}",
      "SMTP_PASSWORD=${smtp_password}",
      'SMTP_SERVER=smtp.gmail.com',
      'SMTP_PORT=587',
      'SMTP_AUTHENTICATION=plain',
      'SMTP_ENABLE_STARTTLS_AUTO=true',
      "EMAIL_FROM_ADDRESS=${email_from_address}",
      'DATABASE_ADAPTER=postgresql',
      'DATABASE_NAME=postgres',
      'DATABASE_USERNAME=postgres',
      'DATABASE_PASSWORD=postgres',
      'DATABASE_HOST=huginn-db',
    ],
  }

  electricgrey::service_proxy { 'huginn':
    port              => 3000,
  }
}
