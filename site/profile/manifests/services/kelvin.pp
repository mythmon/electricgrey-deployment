# == Class: profile::services::kelvin
#
# Profile for a Kelvin server running in Docker, proxied through Nginx.
#
class profile::services::kelvin (
  Optional[String] $htpasswd_contents = undef,
) {
  $kelvin_user_id = 2005;

  user { 'kelvin':
    ensure  => present,
    uid     => $kelvin_user_id,
    gid     => $kelvin_user_id,
    groups  => ['incoming'],
    require => Group['kelvin'],
  }

  group { 'kelvin':
    ensure => present,
    gid    => $kelvin_user_id,
  }

  file { '/var/kelvin':
    ensure => directory,
    owner  => 'kelvin',
    group  => 'kelvin',
    mode   => '0755',
    require => User['kelvin'],
  }

  docker::image { 'stefanwichmann/kelvin':
    image_tag => 'latest',
  }

  docker::run { 'kelvin-web':
    image   => 'stefanwichmann/kelvin',
    ports   => ['9001:8080'],
    env     => [
      'TZ=America/Los_Angeles',
    ],
    volumes => [
      '/var/kelvin:/etc/opt/kelvin',
    ],
    require => File['/var/kelvin'],
  }

  electricgrey::service_proxy { 'kelvin':
    port              => 9001,
    htpasswd_contents => $htpasswd_contents,
    require           => Docker::Run['kelvin-web'],
  }
}
