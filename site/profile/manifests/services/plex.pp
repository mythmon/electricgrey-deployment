# == Class: profile::services::plex
#
# Profile for a plex server proxied through Nginx.
#
class profile::services::plex (
  Optional[String] $htpasswd_contents = undef,
) {
  ::electricgrey::service_proxy { 'plex':
    port              => 32400,
    htpasswd_contents => $htpasswd_contents,
  }

  ::electricgrey::service_proxy { 'plexpy':
    port              => 8181,
    htpasswd_contents => $htpasswd_contents,
  }

  # TODO manage plex package. It's in AUR.
}
