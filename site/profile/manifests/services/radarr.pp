# == Class: profile::services::radarr
#
# Profile for a Radarr server running in Docker, proxied through
# Nginx.
#
class profile::services::radarr (
  Optional[String] $htpasswd_contents = undef,
) {

  $radarr_user_id = 2001;

  user { 'radarr':
    ensure  => present,
    uid     => $radarr_user_id,
    gid     => $radarr_user_id,
    groups  => ['video'],
    require => Group['radarr'],
  }

  group { 'radarr':
    ensure => present,
    gid    => $radarr_user_id,
  }

  file { '/data/video/movies':
    ensure => directory,
    owner  => 'radarr',
    group  => 'video',
    mode   => '0775',
  }

  file { '/var/radarr':
    ensure => directory,
    owner  => 'radarr',
    group  => 'radarr',
    mode   => '0755',
  }

  file { '/var/radarr/config':
    ensure => directory,
    owner  => 'radarr',
    group  => 'radarr',
    mode   => '0755',
  }

  docker::image { 'linuxserver/radarr':
    image_tag => 146,
  }

  docker::run { 'radarr':
    image   => 'linuxserver/radarr',
    ports   => ['7878:7878'],
    env     => [
      "PUID=${radarr_user_id}",
      "PGID=${radarr_user_id}",
    ],
    volumes => [
      '/var/radarr/config:/config',
      '/data/incoming:/downloads',
      '/data/video/movies:/movies',
      '/etc/localtime:/etc/localtime:ro',
    ],
  }

  electricgrey::service_proxy { 'radarr':
    port              => 7878,
    htpasswd_contents => $htpasswd_contents,
  }
}
