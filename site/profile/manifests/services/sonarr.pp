# == Class: profile::services::sonarr
#
# Profile for a sonarr server proxied through Nginx.
#
class profile::services::sonarr (
  Optional[String] $htpasswd_contents = undef,
) {
  ::electricgrey::service_proxy { 'sonarr':
    port              => 8989,
    htpasswd_contents => $htpasswd_contents,
  }

  # TODO manage sonarr package. It's in AUR.
}
