# == Class: profile::gitserver
#
# Profile for a Git server running in Docker, with data in /var/git.
#
class profile::gitserver {

  user { 'git':
    ensure => present,
  }

  file { '/var/git':
    ensure => directory,
    owner  => 'git',
    group  => 'git',
    mode   => '0775',
  }

  file { '/var/git/repos':
    ensure => directory,
    owner  => 'git',
    group  => 'git',
    mode   => '0775',
  }

  file { '/var/git/keys':
    ensure => directory,
    owner  => 'git',
    group  => 'git',
    mode   => '0750',
  }

  docker::image { 'jkarlos/git-server-docker':
    image_tag => 'latest',  # no tags available
  }

  docker::run { 'gitserver':
    image   =>  'jkarlos/git-server-docker',
    ports   => ['2222:22'],
    volumes => [
      '/var/git/repos:/git-server/repos',
      '/var/git/keys:/git-server/keys',
    ],
  }
}
