# == Class: profile::media
#
# Support directories and users for media server
#
class profile::media {
  $video_group_id = 91;

  group { 'video':
    ensure => present,
    gid    => $profile::media::video_group_id,
  }

  file { '/data/video':
    ensure => directory,
    group  => 'video',
    mode   => '0775',
  }
}
