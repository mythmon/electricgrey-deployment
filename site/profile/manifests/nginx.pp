# == Class: profile::nginx
#
# Config for machines running Nginx
#
class profile::nginx {
  include nginx

  file { 'nginx htpasswd dir':
    ensure => 'directory',
    path   => '/etc/nginx/htpasswd.d',
    owner  => 'http',
    group  => 'http',
    mode   => '0770',
  }

  include certbot
}
