# == Class: profile::docker
#
# Config for machines running Docker
#
class profile::docker {
  include docker
}
