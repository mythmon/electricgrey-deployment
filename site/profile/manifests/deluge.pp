# == Class: profile::gitserver
#
# Profile for a Deluge server running in Docker, using a VPN, with
# config in /var/deluge.
#
#
class profile::deluge (
  Optional[String] $htpasswd_contents = undef,
  String $vpn_config,
) {
  $deluge_user_id = 2002;

  user { 'deluge':
    ensure  => present,
    uid     => $deluge_user_id,
    gid     => $deluge_user_id,
    groups  => ['incoming'],
    require => Group['deluge'],
  }

  group { 'deluge':
    ensure => present,
    gid    => $deluge_user_id,
  }

  file { '/var/deluge':
    ensure => directory,
    owner  => 'deluge',
    group  => 'deluge',
    mode   => '0755',
  }

  file { '/data/incoming/deluge':
    ensure => directory,
    owner  => 'deluge',
    group  => 'incoming',
    mode   => '0775',
  }

  file { '/var/deluge/config':
    ensure => directory,
    owner  => 'deluge',
    group  => 'deluge',
    mode   => '0755',
  }

  file { '/var/deluge/config/openvpn':
    ensure => directory,
    owner  => 'deluge',
    group  => 'deluge',
    mode   => '0755',
  }

  file { '/var/deluge/config/openvpn/config.ovpn':
    ensure  => file,
    owner   => 'deluge',
    group   => 'deluge',
    mode    => '0750',
    content => $vpn_config,
    before  => Docker::Run['deluge'],
  }

  docker::image { 'binhex/arch-delugevpn':
    image_tag => '1.3.15_14_gb8e5ebe82-1-20',
  }

  docker::run { 'deluge':
    image            => 'binhex/arch-delugevpn',
    require          => Docker::Image['binhex/arch-delugevpn'],
    extra_parameters => ['--cap-add=NET_ADMIN'],
    ports            => [
      '8112:8112',
      '8118:8118',
      '58846:58846',
      '58946:58946',
    ],
    volumes          => [
      '/data/incoming/deluge:/data',
      '/var/deluge/config:/config',
      '/etc/localtime:/etc/localtime:ro',
    ],
    env              => [
      'VPN_ENABLED=yes',
      'VPN_PROV=custom',
      'VPN_CONFIG=/config/openvpn/config.ovpn',
      'ENABLE_PRIVOXY=no',
      'LAN_NETWORK=192.168.1.1/24',
      'NAME_SERVERS=209.222.18.222,37.235.1.174,8.8.8.8,209.222.18.218,37.235.1.177,8.8.4.4',
      'DEBUG=false',
      'UMASK=000',
      "PUID=${deluge_user_id}",
      "PGID=${deluge_user_id}",
    ],
  }

  electricgrey::service_proxy { 'deluge':
    port              => 8112,
    htpasswd_contents => $htpasswd_contents,
  }
}
